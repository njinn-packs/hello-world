"""
Njinn action example
"""

# You can import packages that are in the python standard library
# or in libraries listed in your requirements.txt
from cowsay import cow


class Greet:
    """
    Greets someone using the cowsay library
    """

    # provide sensible defaults for all inputs
    person = "user"
    translation = {
        "hello": "hello",
        "goodbye": None,
    }

    def greet(self, greeting):
        """
        You are free to define other methods.
        """
        if "{}" in greeting and self.person is not None:
            cow(greeting.format(self.person))
        else:
            cow(greeting)

    def run(self):
        """
        Implement the run() method on your class to create a custom action.
        """
        if self.person == "Rabbit of Caerbannog":
            raise ValueError("Not greeting villains!")
        self.greet(self.translation["hello"])
        goodbye = self.translation.get("goodbye")
        if goodbye is not None:
            greeting_count = 2
            self.greet(goodbye)
        else:
            greeting_count = 1
        return { "greeting_count" : greeting_count }