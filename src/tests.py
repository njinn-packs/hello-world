import unittest
from greet import Greet

class TestGreeting(unittest.TestCase):

    def test_two_greetings(self):
        action = Greet()
        action.person = "test user"
        action.translation = {
            "hello": "Hi, {}",
            "goodbye": "Bye."
        }
        assert action.run() == { "greeting_count" : 2 }
    def test_one_greetings(self):
        action = Greet()
        action.person = "test user"
        action.translation = {
            "hello": "Hi, {}"
        }
        assert action.run() == { "greeting_count" : 1 }

if __name__ == '__main__':
    unittest.main()