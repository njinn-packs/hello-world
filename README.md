# Njinn Action Development Example

This repository contains a toy example of an Njinn action demonstrating most of the features of Njinn actions:
* An *action* using an external library: The *Greet someone* action outputs predefined greetings addressed to a person to stdout, using [`cowsay`](https://pypi.org/project/cowsay/) for fomatting. The number of greetings printed to `stdout` is returned as the action result, accessible for further workflow steps.
```
  ___________
< Hello, user >
  ===========
                \
                 \
                   ^__^                             
                   (oo)\_______                   
                   (__)\       )\/\             
                       ||----w |           
                       ||     ||  
```
* A definition for a *config scheme* which acts as part of the input for the action. In this example, we can store *translations* of "hello" (mandatory) and "goodbye" (optional).
* All the metadata required to deploy the action and config to an Njinn tenant.


## Repository Contents
* A `Pipfile` created with `pipenv --python 3.7 "cowsay==2.0.3"` ensuring development inside a Python 3.7 environment using an example library.
* The `requirements.txt` file which is used to create a vitualenv on the njinn worker, in which the action is executed.
* A `pack.yaml` file containing
  * A config scheme and
  * an action using this config scheme as well as a plain string parameter as inputs.
* `src/greet.py` containing the actual action class, `Greet`.

## How It Works
* At the heart of the action is the `pack.yml` file with general metadata about the pack and two important sections:
  * `config-schemes` contains config schemes. These allow you to define custom input schemes for *Config* objects that can be re-used in different workflows. The definition of the schemes are inspired by [JSON Schema](https://json-schema.org/) with a few small changes: `required` is an attribute of properties, a custom property type called `secret` which is encrypted in Njinn and never displayed in the UI, an `order` property used to order the input fields in the Njinn UI, and the ability to define an icon for the scheme.
  * `actions` defines the actions. Its key becomes its name, which has to be unique inside the pack. There is a title, an icon and a path. The path `src.greet:Greet` refers to the class `greet` inside `src/greet.py`. If the path changes, all workflows using the action need to be saved once for the change to be applied. Its inputs can either be defined directly on the action, like `person`, or refer to config schemes, like `translation`.
* The class `Greet` implements the action. Its inputs correspond to those in the `pack.yml`. It implements a `run()` method, handling cases where optional inputs are not available. In case of success, it returns a result `dict` which will be available in later steps of the workflow. Returning a result is optional. To signal failure, it can throw an `Exception`, in this case `ValueError`.

### Input Fields
Variables for Actions are defined under its `parameters` in the `pack.yml` and are used to configure the tasks in workflows. Variables for Config-Schemes follow the same principle and are defined under its `properties` in the `pack.yml`. Those variables are defined as objects in the `pack.yml`. The object key / name is used as the variable name. The definition may contain several attributes in order to change the appearance of the input forms. Each variable requires at least a name, title and type.

| Property    | Type           | Required | Allowed for type              | Description                                                                                                                                                                                                                                                    |
|-------------|----------------|----------|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| type        | string         | yes      |                               | Can be one of the following values:<br>- string<br>- integer<br>- number (equivalent with float)<br>- boolean<br>- text<br>- code (shows a code editor)<br>- secret (stored encrypted)<br>- select<br>- object (nested parameters as properties of the object) |
| title       | string         | yes      | all                           | Title to be displayed for the variable in the UI.                                                                                                                                                                                                              |
| description | string         | no       | all                           | Description on purpose of the variable, how to use it and examples.                                                                                                                                                                                            |
| default     | string         | no       | all                           | Default value.                                                                                                                                                                                                                                                 |
| required    | boolean        | no       | all                           | Defaults to false if not present. If set to true, the value cannot be empty.                                                                                                                                                                                   |
| order       | int            | no       | all                           | Specifies the order to list the fields in. Showing from lowest to highest value. The values do not need to be sequential. Same values are unordered.<br>Example values: 1, 4, 10, 20, 20, 22                                                                   |
| placeholder | string         | no       | string, text, number, integer | Rendered as a placeholder when the value is empty. Should represent the default value in the Action code.                                                                                                                                                      |
| enum        | list           | no       | string, select                | The value is only allowed to be one of the given list in enum.                                                                                                                                                                                                 |
| min_length  | integer        | no       | string, text, secret          | The minimum length for strings, text and secret.                                                                                                                                                                                                               |
| max_length  | integer        | no       | string, text, secret          | The maximum length for strings, text and secret.                                                                                                                                                                                                               |
| min         | integer, float | no       | number, integer               | The minimum numeric value                                                                                                                                                                                                                                      |
| max         | integer, float | no       | number, interger              | The maximum numeric value                                                                                                                                                                                                                                      |
| lang        | string         | no       | code                          | language for code higlighting for inputs of type code                                                                                                                                                                                                          |

## How to Develop
* Create a git repository to version your action pack in.
* Develop your action pack
  * We recommend using some sort of IDE.
  * Having a local virtualenv with the pack's dependencies helps your IDE support you better.
  * Optional: You can test your actions locally by using a testing framework like shown in `src/test.py` (recommended), or loading it as a module from a separate file and running it directly.
* Upload the action to your tenant via the [Njinn client](https://pypi.org/project/njinn/). For example, if your tenant name is `example` and the repository URL is `https://gitlab.com/njinn-packs/hello-world.git`, you can install the pack via `njinn pack install -a https://example.njinn.io https://gitlab.com/njinn-packs/hello-world.git`. Repeating this process will overwrite previous versions, and new executions will use the current version.
  * For deployment using configuration management tools, you can provide username and password via the environment variables `NJINN_USER` and `NJINN_PASS`.
After finishing these steps, you can create configs and workflows using the config schemes and actions defined in the action pack.

## Things to try
* Split the `Greet` class into a `SayHello` and `SayGoodbye` class. If you change the name of a class or file used in an action, you have to save the workflows using it for the changes to be applied.
* Change the icons - you can use the free icons from [https://fontawesome.com/](https://fontawesome.com/)
* Change the result dictionary.
